//
//  main.m
//  BuildAutomation
//
//  Created by Francisco Javier Medina Bravo on 11/30/16.
//  Copyright © 2016 Francisco Javier Medina Bravo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
