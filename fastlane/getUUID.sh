#!/bin/bash

export bundleId=$1

if [[ -n "$bundleId" ]]; then

FILES=../profiles/*.mobileprovision
for f in $FILES
do
BUNDLE_IDENTIFIER=`egrep -a -A 2 application-identifier $f | grep string | sed -e 's/<string>//' -e 's/<\/string>//' -e 's/ //' | awk '{split($0,a,"."); i = length(a); for(ix=2; ix <= i;ix++){ s=s a[ix]; if(i!=ix){s=s "."};} print s;}'`
if [ "$bundleId" == "$BUNDLE_IDENTIFIER" ]; then
uuid=`grep UUID -A1 -a $f | grep -io "[-A-Z0-9]\{36\}"`
echo "$uuid"
fi
done

else
echo "*"
fi
