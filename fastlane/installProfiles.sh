#!/bin/bash

#echo "Installing certificates on local keychain"
#CERTS=../Certificate/*.cer
#for ce in $CERTS
#do
#echo "adding certificate $ce"
#sudo security add-trusted-cert -d -r trustAsRoot -k "/Library/Keychains/System.keychain" $ce
#done

echo "Installing provisionig profiles"
FILES=../profiles/*.mobileprovision
for f in $FILES
do
uuid=`grep UUID -A1 -a $f | grep -io "[-A-Z0-9]\{36\}"`
echo "copying profile with uuid: '$uuid'"
cp $f ~/Library/MobileDevice/Provisioning\ Profiles/$uuid.mobileprovision
done
